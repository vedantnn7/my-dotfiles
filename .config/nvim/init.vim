" Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!
NeoBundle 'itchyny/lightline.vim'
NeoBundle 'ycm-core/YouCompleteMe'
NeoBundle 'preservim/nerdtree'
NeoBundle 'ap/vim-css-color'
NeoBundle 'arcticicestudio/nord-vim'
" NeoBundle 'vim-airline/vim-airline'
" NeoBundle 'vim-airline/vim-airline-themes'

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck 

" ------- Custom Config -------

let g:lightline = {
      \ 'colorscheme': 'nord',
	\ }

set mouse=a
set clipboard=unnamedplus
set number
set noswapfile
colorscheme nord
" nmap <F8> :TagbarToggle<CR>

" @airline
" let g:airline = 1 "Open tabline or not"
" let g:airline_powerline_fonts = 1
" let g:airline_theme='violet' 
" let g:airline_left_sep = '▶'
" let g:airline_left_alt_sep = '❯'
" let g:airline_right_sep = '❮'
" let g:airline_right_alt_sep = '❮'

set encoding=utf8

