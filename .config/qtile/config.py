from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

import subprocess
import os

mod = "mod4"
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "control"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "control"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "control"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "control"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "shift"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "shift"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "shift"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "shift"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod, "shift"], "x", lazy.spawn("betterlockscreen --lock -l dim -t --blur --text 'Taken a Break??'")),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "x", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", qtile.cmd_spawn("./.config/dmscripts/scripts/dm-logout.sh")),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "p", lazy.spawn("rofi -show drun"),
        desc="Spawn a command using a prompt widget"),

    Key(["control", "shift"], "e", lazy.spawn("emacs"), desc="Launch Favoraite Editor"),
    Key(["control", "shift"], "b", lazy.spawn("firefox-developer-edition"), desc="Launch Favoraite Browser"),
    # Key(["control", "shift"], "d", lazy.spawn("emacsclient -c -a 'emacs' --eval '(dired nil)'")),
    Key(["control", "shift"], "d", lazy.spawn("pcmanfm")),
    Key(["control", "mod1"], "d", lazy.spawn("zathura")),
    Key(["mod1", "shift"], "w", lazy.spawn("dm-wifi")),
    Key(["mod1", "shift"], "d", lazy.spawn("killall dunst")),
    Key(["mod1", "control"], "d", lazy.spawn("dunst &")),
    Key([], "Print", lazy.spawn("flameshot launcher")),
    Key(["control", "shift"], "w", lazy.spawn("nitrogen"))
]


group_names = [
    ("一", {'layout': 'monadtall'}),
    ("二", {'layout': 'columns'}),
    ("三", {'layout': 'columns'}),
    ("四", {'layout': 'monadtall'}),
    ("六", {'layout': 'monadtall'}),
    ("七", {'layout': 'monadtall'}),
    ("八", {'layout': 'floating'}),
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {
        "border_width": 2,
        "margin": 8,
        "border_focus": "#88c0d0",
        "border_normal": "#5e81ac",
}

layouts = [
    layout.Columns(**layout_theme),
    layout.Max(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Tile(**layout_theme),
    layout.Floating(**layout_theme)
]

last_playing = 'spotify'

def playpause(qtile):
    global last_playing
    if qtile.widgetMap['clementine'].is_playing() or last_playing == 'clementine':
        qtile.cmd_spawn("clementine --play-pause")
        last_playing = 'clementine'
    if qtile.widgetMap['spotify'].is_playing or last_playing == 'spotify':
        qtile.cmd_spawn("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause")
        last_playing = 'spotify'

def next_prev(state):
    def f(qtile):
        if qtile.widgetMap['clementine'].is_playing():
            qtile.cmd_spawn("clementine --%s" % state)
        if qtile.widgetMap['spotify'].is_playing:
            cmd = "Next" if state == "next" else "Previous"
            qtile.cmd_spawn("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.%s" % cmd)
    return f

colors = [
    "#2e3440", 
    "#7ba4d7",
    "#8fbcbb",
    "#4e3440"
]

widget_defaults = dict(
    font='Fira Sans',
    fontsize=12,
    background=colors[0],
    foreground = colors[1],
    padding=4,
    highlight_color= colors[2],
    margin=10
)
extension_defaults = widget_defaults.copy()

sep_theme = {
    'padding': 18,
    'size_percent': 40,
    'foreground': colors[2]
}

bottom_bar_margin = 550 

screens = [
    Screen(
        top=bar.Bar(
            [
                ################## LEFT SIDE ######################
                widget.Sep(padding=5, size_percent= 0),
                widget.TextBox(
                    "",
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -show drun")},
                    foreground = colors[2],
                    fontsize= 14,
                    margin= 10
                ),
                widget.Sep(**sep_theme),
                # widget.Mpris2(
                #    name='spotify',
                #    objname="org.mpris.MediaPlayer2.spotify",
                #    display_metadata=['xesam:title', 'xesam:artist'],
                #    scroll_chars=None,
                #    stop_pause_text=' Paused',
                # ),
                widget.Cmus(),

                ################## CENTER SIDE ######################
                widget.WindowName(
                    opacity = 0,
                    foreground = colors[0],
                ),
                widget.GroupBox(
                       active = colors[1],
                       hide_unused = True,
                       rounded = False,
                       highlight_color = colors[0],
                       highlight_method = "text",
                       disable_drag = True,
                       this_current_screen_border = colors[2],
                       this_screen_border = colors[2],
                ),
                ################# RIGHT SIDE ########################
                widget.WindowName(
                    opacity = 0,
                    foreground = colors[0],
                ),

                
                widget.Net(
                       interface = "wlan0",
                       format = ' {down}',
                       foreground = colors[2],
                       padding = 5
                ),
                widget.Sep(**sep_theme),
                widget.CurrentLayout(
                    margin=2,
                ),
                widget.Sep(**sep_theme),
                widget.TextBox(
                    "",
                    foreground = colors[2],
                    fontsize = 16,
                ),
                widget.Volume(
                    foreground = colors[2],
                ),
                widget.Sep(**sep_theme),
 
                widget.TextBox(
                    "" ,
                    foreground = colors[1],
                    fontsize = 16,
                ),               
                widget.Clock(
                    format = "%a, %I:%M",
                ),
                widget.Sep(**sep_theme),
                widget.Systray(background = colors[0], foreground=colors[0]),
                widget.Sep(padding=5, size_percent= 0),
            ],
            28,
            padding=[20 , 20, 20, 20],
            margin=[12, 12, 0, 12],  # N E S W
            background=colors[0],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    **layout_theme,
    float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Pcmanfm'),
    Match(wm_class='GIMP'),
    Match(wm_class='Nitrogen'),
    Match(wm_class="kdenlive"),
    Match(wm_class="evince"),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

#########################################################
################ assgin apps to groups ##################
#########################################################
@hook.subscribe.client_new
def assign_app_group(client):
     d = {}
     #####################################################################################
     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
     #####################################################################################
     d[group_names["web"]] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
     d[group_names["dev"]] = [ "Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
                "atom", "subl3", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
     d[group_names["gfx"]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
     d[group_names["gfx"]] = ["Gimp", "gimp" ]
     d[group_names["doc"]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
     d[group_names["vid"]] = ["Vlc","vlc", "Mpv", "mpv" ]
     d[group_names["chat"]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
               "virtualbox manager", "virtualbox machine", "vmplayer", ]
     d[group_names["doc"]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
     d[group_names["chat"]] = ["Evolution", "Geary", "Mail", "Thunderbird",
               "evolution", "geary", "mail", "thunderbird" ]
     d[group_names["mus"]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
     ######################################################################################

     wm_class = client.window.get_wm_class()[0]

     for i in range(len(d)):
         if wm_class in list(d.values())[i]:
             group = list(d.keys())[i]
             client.togroup(group)
             client.group.cmd_toscreen(toggle=False)

@hook.subscribe.startup_once
def start_once():
   home = os.path.expanduser('~')
   subprocess.call([home + '/.config/qtile/autostart.sh'])


wmname = "LG3D"
