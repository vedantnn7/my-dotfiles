#! /bin/bash 
picom &
xautolock -time 10 -locker nitrogen --set-zoom-fill --random ~/Pictures/Nord\ Wallpapers/ --save &
lxsession &
dunst &
xautolock -time 10 -locker betterlockscreen --lock -l dim -t --blur --text "Taken A Break?"
redshift &
killall nm-applet &
nm-applet --indicator
nitrogen --set-zoom-fill --random ~/Pictures/Wallpapers/ --save &
festival --tts ./welcome.txt

