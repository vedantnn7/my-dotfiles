source ("/usr/bin/starship" init fish --print-full-init | psub)
alias ls="exa -lah"
alias cat="bat"
alias vim="nvim"
alias v="vim"
alias upd="sudo pacman -Syu"
alias gits="git status"
alias gc="git commit"
alias ga="git add"

colorscript random
