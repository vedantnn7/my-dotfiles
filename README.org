#+TITLE: Vedant's Dotfiles
* My Dotfiles
This is the backup of customization of the programs that I use daily.

** Programs that I use often
- [[https://gitlab.com/vedantnn7/my-dotfiles/-/tree/master/.config][Qtile]] ( window manager )
- [[https://gitlab.com/vedantnn7/my-dotfiles/-/tree/master/.config/alacritty][Alacritty]] ( terminal emulator )
- [[https://gitlab.com/vedantnn7/my-dotfiles/-/tree/master/.config/nvim][Neovim]] ( text editor )
- [[https://gitlab.com/vedantnn7/my-dotfiles/-/tree/master/.config/fish][Fish]] ( shell )
- [[https://gitlab.com/vedantnn7/my-dotfiles/-/tree/master/.config/starship.toml][Starship]] ( prompt )

** Some screenshots
[[https://gitlab.com/vedantnn7/my-dotfiles/-/raw/master/.config/qtile/qtile.jpg][My qtile customization
]]
[[https://gitlab.com/vedantnn7/dmenu-vedantnn7/-/raw/master/dmenu.jpg][My Dmenu Config
]]
[[https://gitlab.com/vedantnn7/my-dotfiles/-/raw/master/.config/fish/fish.jpg][My Fish Config]]

If you want to reslove any bug or improve my customization, contributions are welcome.
